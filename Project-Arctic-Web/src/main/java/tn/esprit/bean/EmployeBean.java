package tn.esprit.bean;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tn.esprit.entities.Employe;
import tn.esprit.entities.Ingenieur;
import tn.esprit.entities.Nationalite;
import tn.esprit.entities.Technicien;
import tn.esprit.services.EmployeService;
import tn.esprit.services.IEmployeLocal;

@ManagedBean
@ViewScoped
public class EmployeBean {

	@EJB
	private IEmployeLocal employerservice;
	private boolean etat = false;
	private String typeEmploye;
	
	private String name;
	private String lastname;
	private Nationalite nationalite;
	
	private Date dateNaissance;
	
	private String login;
	
	private Nationalite[] nationalites;

	private Employe employe;

	private List<Employe> employes;

	@PostConstruct
	public void init() {
		employes = employerservice.findAllEmploye();
		nationalites = Nationalite.values();
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	public List<Employe> getEmployes() {
		return employes;
	}

	public void setEmployes(List<Employe> employes) {
		this.employes = employes;
	}

	public void preUpdate(Employe e) {
		etat = true;
		employe = employerservice.findEmployeById(e.getId());
		employe.setName(e.getName());
		employe.setLastName(e.getLastName());
		employe.setNationalite(e.getNationalite());
		employe.setDateNaissance(e.getDateNaissance());
		employe.setLogin(e.getLogin());

	}

	public void update() {
		employerservice.updateemploye(employe);
		employes = employerservice.findAllEmploye();
		etat = false;
	}

	public void delete(int id) {
		employerservice.deleteemploye(id);
		for (Employe e : employes) {
			if (e.getId() == id) {
				employes.remove(e);
				break;
			}
		}
	}
	
	public String add(){
		
		if (typeEmploye.equals("ing")) {
			Ingenieur ing= new Ingenieur();
			ing.setName(name);
			ing.setLastName(lastname);
			ing.setLogin(login);
			ing.setDateNaissance(dateNaissance);
			ing.setNationalite(nationalite);
			employerservice.addEmploye(ing);
		}
		else if (typeEmploye.equals("tech")) {
			Technicien tech = new Technicien();
			tech.setName(name);
			tech.setLastName(lastname);
			tech.setLogin(login);
			tech.setDateNaissance(dateNaissance);
			tech.setNationalite(nationalite);
			employerservice.addEmploye(tech);
		}
		typeEmploye="";
		name= "";
		lastname="";
		login = "";
		dateNaissance = null;
		nationalite = null;
		return null;
		
	}

	public boolean isEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	public Nationalite[] getNationalites() {
		return nationalites;
	}

	public void setNationalites(Nationalite[] nationalites) {
		this.nationalites = nationalites;
	}

	public String getTypeEmploye() {
		return typeEmploye;
	}

	public void setTypeEmploye(String typeEmploye) {
		this.typeEmploye = typeEmploye;
	}
	
	public Nationalite getNationalite() {
		return nationalite;
	}

	public void setNationalite(Nationalite nationalite) {
		this.nationalite = nationalite;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	
}
