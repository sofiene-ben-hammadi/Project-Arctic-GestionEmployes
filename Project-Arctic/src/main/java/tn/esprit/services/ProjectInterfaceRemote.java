package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

import tn.esprit.entities.Employe;
import tn.esprit.entities.Projet;

@Remote
public interface ProjectInterfaceRemote {

	public void addProject(Projet p);

	public Projet updateProjet(Projet p);

	public void deleteProjet(Projet p);

	public List<Projet> getAllProjets();

	public Projet getProjetById(int id);

	public List<Projet> getProjetsByResponsable(Employe e);
	public List<Projet> getProjetsByEmploye(Employe e);
	
}
