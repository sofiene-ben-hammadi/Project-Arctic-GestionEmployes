package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Employe;
import tn.esprit.entities.Projet;

@Local
public interface ProjectInterfaceLocal {

	public void addProject(Projet p);

	public Projet updateProjet(Projet p);

	public void deleteProjet(Projet p);

	public List<Projet> getAllProjets();

	public List<Projet> getProjetsByResponsable(Employe e);

	
}
