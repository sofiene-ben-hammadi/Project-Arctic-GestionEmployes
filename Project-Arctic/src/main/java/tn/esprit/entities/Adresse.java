package tn.esprit.entities;

import javax.persistence.Embeddable;

@Embeddable
public class Adresse {

	private String ville;
	private String rue;
	private int numeroMaison;
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public int getNumeroMaison() {
		return numeroMaison;
	}
	public void setNumeroMaison(int numeroMaison) {
		this.numeroMaison = numeroMaison;
	}
}
