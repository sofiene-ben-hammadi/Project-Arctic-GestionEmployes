package tn.esprit.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Tache  implements Serializable{
 
	@EmbeddedId
	private TachePK tachePK;
	
	@ManyToOne
	@JoinColumn(name="idprojet",referencedColumnName="id",insertable=false,updatable=false)
	private Projet projet;

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	public TachePK getTachePK() {
		return tachePK;
	}

	public void setTachePK(TachePK tachePK) {
		this.tachePK = tachePK;
	}

	@ManyToOne
	@JoinColumn(name="idemploye",referencedColumnName="id",insertable=false,updatable=false)
	private Employe employe;
	
	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	private int dure;

	public int getDure() {
		return dure;
	}

	public void setDure(int dure) {
		this.dure = dure;
	}
	
}

